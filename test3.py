import pytest

class MyApp:
    def __init__(self, version, a, b):
        self.version = version
        self.a = a
        self.b = b
        if not isinstance(a, int) or not isinstance(b, int):
            raise Exception("Not an integer")

    def get_version(self):
        return self.version

    def add(self):
        return self.a + self.b

    def mul(self):
        return self.a * self.b

class TestMyApp:

    def setup(self):
        self.mapp = MyApp(1, 4, 5)

    def test1(self):
        assert 9 == self.mapp.add()

    def test2(self):
        assert 9 == self.mapp.mul()


class TestMyApp1:

    def setup(self):
        self.mapp = MyApp(1, 4, 5)

    def test1_1(self):
        assert 9 == self.mapp.add()

    def test2_1(self):
        assert 9 == self.mapp.mul()
