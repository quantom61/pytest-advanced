import pytest

def validate_string(mstr):
    # This function checks if the input is a string.
    tr = True if isinstance(mstr, str) else False # Ternary Operator
    return tr


def test_str1():
    mystr = "quantom"

    assert validate_string(mystr), "This is a string."


def test_str2():
    mystr = 256

    assert False == validate_string(mystr), "This is not a string."


def test_str3():
    mystr = 256

    assert validate_string(mystr), "This is a string."
